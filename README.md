# nvim_config
A usable .config/nvim
# Install
Install vim plug. Look here to find out how https://github.com/junegunn/vim-plug#Installation

Installl dependencies. The dependencies are powerline, nerdfont, ripgrep, clangd (for c/c++ users), and pylsp (for python users).

Getting the nvim_config installed.

cd ~/.config

git clone https://gitea.com/nathansmithsmith/nvim_config

mv nvim_config nvim

nvim ~/.config/nvim/lua/plugin_management.lua

:PlugInstall

# Keybindings
leaderkey: ,.

alt-e to close nvim-cmp popups.

Open terminal: leader-c

Toggle/close terminal: A-\

Telescope find files: leader-f

Telescope live grep: leader-g

Cmake build: leader-b

Telescope help tags: leader-h

Telescope marks: leader-m

Telescope man_pages: leader-p


# Moving around.
Hold down alt to move to different windows in split screen.

To move around in the nvim-cmp popup, telescope, and dashboard use alt-n for next and alt-p for previous and alt-e to close.

