local autocmd = vim.api.nvim_create_autocmd

local set_cursorline = function()
	local buf = vim.api.nvim_get_current_buf()

	-- Do not set cursorline if telescope is open.
	if vim.bo[buf].filetype == "TelescopePrompt" then
		vim.cmd("set nocul")
	else
		vim.cmd("set cul")
	end
end

autocmd("InsertEnter",
{pattern="*", callback=set_cursorline
})

autocmd("InsertLeave",
{pattern="*", command="set nocul"
})
