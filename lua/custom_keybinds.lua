local autocmd = vim.api.nvim_create_autocmd
local constants = require("constants")
local rickroll = require("rickroll")

-- Leader key.
vim.g['mapleader'] = ",."

-- Next and previous.
vim.keymap.set({"i", "v", "n", "c"}, constants.next_key, "<C-n>")
vim.keymap.set({"i", "v", "n", "c"}, constants.previous_key, "<C-p>")

-- Bye bye arrows
vim.keymap.set({"i", "n", "v"}, "<Right>", rickroll.rickroll)
vim.keymap.set({"i", "n", "v"}, "<Left>", rickroll.rickroll)
vim.keymap.set({"i", "n", "v"}, "<Up>", rickroll.rickroll)
vim.keymap.set({"i", "n", "v"}, "<Down>", rickroll.rickroll)

-- Switching windows.
vim.keymap.set("n", "<A-h>", "<C-w><Left>")
vim.keymap.set("n", "<A-j>", "<C-w><Down>")
vim.keymap.set("n", "<A-k>", "<C-w><Up>")
vim.keymap.set("n", "<A-l>", "<C-w><Right>")

-- Python scripts.
autocmd("Filetype",
{pattern="python", command="nnoremap <silent> <F5> :!python3 %<CR>"
})

-- Shell scripts.
autocmd("Filetype",
{pattern="sh", command="nnoremap <silent> <F5> :!./%<CR>"
})

-- Lua scripts.
autocmd("Filetype",
{pattern="lua", command="nnoremap <silent> <F5> :!lua %<CR>"
})
