local autocmd = vim.api.nvim_create_autocmd

-- Restore cursor position.
autocmd("BufReadPost", 
{pattern="*" , command="silent! normal! g`\"zv"
})
