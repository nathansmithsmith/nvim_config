M = {}

function M.rickroll()
	local handle = io.popen('xdg-open ~/.config/nvim/assets/rickroll.mp4 > /dev/null 2> /dev/null')
	handle:close()
end

vim.api.nvim_create_user_command("Rickroll", M.rickroll, {})

-- Hehehehehehe. If you are reading this griffin JUST USE HJKL!!!
vim.keymap.set({"i", "n", "v"}, "<Right>", M.rickroll)
vim.keymap.set({"i", "n", "v"}, "<Left>", M.rickroll)
vim.keymap.set({"i", "n", "v"}, "<Up>", M.rickroll)
vim.keymap.set({"i", "n", "v"}, "<Down>", M.rickroll)

return M
