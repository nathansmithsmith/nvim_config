local M = {}

local fmt = function(name, context)
	local buflist = vim.fn.tabpagebuflist(context.tabnr)
	local current_tab = vim.api.nvim_get_current_tabpage()
	local winnr = vim.fn.tabpagewinnr(context.tabnr)
	local bufnr = buflist[winnr]
	local mod = vim.fn.getbufvar(bufnr, '&mod')
	local icon = require('nvim-web-devicons').get_icon(name)

	if icon == nil then
		icon = ''
	else
		icon = icon .. ' '
	end

	return icon .. name .. " " .. context.tabnr
end

M.tab_component = {
	"tabs", 
	max_length = vim.o.columns,
	mode = 1,
	fmt = fmt
}

return M
