local custom_header = {
[[                                   ]],
[[                                   ]],
[[          ▀████▀▄▄              ▄█ ]],
[[            █▀    ▀▀▄▄▄▄▄    ▄▄▀▀█ ]],
[[    ▄        █          ▀▀▀▀▄  ▄▀  ]],
[[   ▄▀ ▀▄      ▀▄              ▀▄▀  ]],
[[  ▄▀    █     █▀   ▄█▀▄      ▄█    ]],
[[  ▀▄     ▀▄  █     ▀██▀     ██▄█   ]],
[[   ▀▄    ▄▀ █   ▄██▄   ▄  ▄  ▀▀ █  ]],
[[    █  ▄▀  █    ▀██▀    ▀▀ ▀▀  ▄▀  ]],
[[   █   █  █      ▄▄           ▄▀   ]],
[[                                   ]],
}

local custom_center = {
	{icon = "", desc = " Find file", action = "Telescope find_files"},
	{icon = "", desc = " Search Text", action = "Telescope live_grep"},
	{icon = "", desc = " Recent Files", action = "Telescope oldfiles"},
	{icon = "", desc = " Telescope", action = "Telescope"},
	{icon = "", desc = " Terminal", action = "ToggleTerm"},
	{icon = "󰘥", desc = " Help", action = "Telescope help_tags"}
}

require("dashboard").setup {
	theme = "doom",
	config = {
		header = custom_header,
		center = custom_center,
		footer = {"I spend to much time doing this )-:"},
	},

    hide = {
      statusline = true,
      tabline = true,
      winbar = true
    },
}
