require("plugin_management") -- Plugin management should always go first.
require("custom_keybinds")
require("filetype_handing")
require("config_options")

-- Not a rickroll
-- https://www.youtube.com/watch?v=hvL1339luv0

-- Config plugins.
require("rickroll")
require("config_cmp")
require("config_navic")
require("config_toggleterm")
require("config_nvim_tree")
require("config_telescope")
require("config_lualine")
require("config_auto_session")
require("config_cmake")
require("config_custom_dashboard")
require("config_ouroboros")

require("cursorline")
require("vim_behavior")
